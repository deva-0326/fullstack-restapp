details();

function details() {
    var restaurant_id = document.getElementById('restaurant_id').innerHTML;

    var output = {
        "async": true,
        "crossDomain": true,
        "url": "https://developers.zomato.com/api/v2.1/restaurant?res_id=" + restaurant_id,
        "method": "GET",
        "headers": {
            "user-key": "3c72ffc62f24a4e6447a2b3b2c6f41ec",
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    $.getJSON(output, function (data) {
        res_name = data.name;
        res_address = data.location.address;
        res_timings = data.timings;
        res_thumb = data.thumb;
        res_phoneNumbers = data.phone_numbers;

        var node = "";
        node+= "<h5 class='card-header'><a data-toggle='collapse' href='#collapse-example' aria-expanded='true' aria-controls='collapse-example' id='heading-example' class='d-block'>"+res_name+"</a></h5>"
        node+="  <div id='collapse-example' class='collapse hide' aria-labelledby='heading-example'>"
        node+=" <div class='card_left'> <div class='img-square-wrapper'> <img class='resimg img-rounded' src="+ res_thumb +" alt='Card image cap'height='185' width='185'></div>"
        node+=" <div class='card-body'><h4 class='card-title'>"+res_name+"</h4> <p class='card-text'>"+res_address+"</p> <span class='badge badge-pill badge-success rating_position'></span></div></div>"
        // node+=""
        node += "</div>";
        $('#content').html(node);
    });
};
