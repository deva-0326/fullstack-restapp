// redirect to search_site
function stepin() {
    window.open("http://localhost:4000/restaurant");
}

// fetch data using api
$(document).ready(function () {

    $("#search_btn").on("click", function () {
        var searchbox = $('#search_value').val()
        if (searchbox === "") {
            return;
        }
        dynamicStore();
    });

    function dynamicStore() {
        var dropdown = $('#select_box').val();
        var searchbox = $('#search_value').val()
        var searchcity = "&q=" + searchbox;
        var restaurant_id;
        var jsonoutput = {
            "async": true,
            "crossDomain": true,
            "url": "https://developers.zomato.com/api/v2.1/search?entity_id=" + dropdown + "&entity_type=city" + searchcity + "&count=20",
            "method": "GET",
            "headers": {
                "user-key": "d710754ce67200fb6fb9b5e26139f50e",
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        $.getJSON(jsonoutput, function (data) {
            localStorage.setItem('myStorage', JSON.stringify(data));
            var retrieve_data = JSON.parse(localStorage.getItem('myStorage'));
            data = retrieve_data.restaurants;
            var tag = "";
            $.each(data, function (index, value) {
                var fetch = data[index];
                $.each(fetch, function (index, value) {
                    var location = fetch.restaurant.location;
                    var userRating = fetch.restaurant.user_rating;
                    restaurant_id = fetch.restaurant.id;
                    tag += "<div class='content img-rounded' id='dynamic_id'>";
                    tag += "<div class='label'>";
                    tag += "<span title='" + userRating.rating_text + "'><p style='color:white;background-color:#" + userRating.rating_color + ";border-radius:4px;border:none;padding:2px 10px 2px 10px;text-align: center;text-decoration:none;display:inline-block;font-size:16px;float:right;'><strong>" + userRating.aggregate_rating + "</strong></p></span><br>";
                    tag += "  <strong class='text-info'>" + userRating.votes + " votes</strong>";
                    tag += "</div>";
                    tag += "<img class='thumb_img img-rounded' src=" + value.thumb + " alt='Restaurant Image' height='155' width='185'>";
                    tag += "<a href='http://localhost:4000/results?key=" + restaurant_id +"'><h2 style='color:red;'><strong>" + value.name + "</strong></h2></a>";
                    tag += "  <h6 style='color:grey;'><strong>" + location.address + "</strong></h6><hr>";
                    tag += "  <strong>CUISINES</strong>: " + value.cuisines + "<br>";
                    tag += "  <strong>COST FOR TWO</strong>: " + value.currency + value.average_cost_for_two + "<br>";
                    tag += "</div><br>";
                });
            });
            $(".dynamic_content").html(tag);
        });
    }

    $("#select_box").change(function () {
        dynamicStore();
    });
});


