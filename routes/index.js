var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/restaurant', function(req, res, next) {
  res.render('restaurant');
});

router.get('/results', function(req, res, next) {
  res.render('search_site',{
    data:req.query.key
  });
});

module.exports = router;
